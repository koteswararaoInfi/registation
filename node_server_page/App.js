const express = require('express');
const cors = require('cors');
const fs = require('fs');
const bodyParser = require('body-parser');
const app = express();
const PORT = 3000;
var data = {}
 data.table =[];
app.use(bodyParser.urlencoded({
    extended: false
 }));
app.use(bodyParser.json());
app.use(cors());
const router = new express.Router();

app.post('/api',(req,res) => {
   
    data.table.push(JSON.stringify(req.body,'utf8',2));
    fs.writeFileSync('form.json', JSON.stringify(data), function(err, result) {
        if(err) console.log('error', err);
        res.send(req.body)
        
      });
});
app.get('/api/get',(req,res) => {
    fs.readFile("form.json", 'utf8', function(err, data_file){
        if(err) throw err;
         res.send(data_file);
    });
 });

app.listen(PORT, () => {
    console.log(`Server started at: http://localhost:${PORT}`);
});